ExportSimProteusTest

To recreate test:
File-Open simplebox.cmb from testing-data/model/3d/cmb
File-Open ProteusPoisson.crf template from testing-data/Proteus
Switch to Attribute tab

Select "FEM" tab
Check "Lump Mass Matrix"
Select "Tolerances" tab
Click New
Select "Miscellaneous" tab
Select "Boundary Force" attribute
Click New
Switch to "Physics" tab
Switch to "Boundary Conditions" tab
Select "Flux boundary Conditions"
Click New
Click below scrollbar
Assign Face1, then Face4

File-Export Simulation File
Select "Advanced" level
Select "ProteusPoisson.py" from testing-data/simulation_workflow/Proteus as "Python Script"
Save to test_p.py in the testing-data folder.
Save to test_n.py in the testing-data folder.
 * Note: Manually edit the script afterward to point these two files to testing/Temporary
Export and Stop Recording
